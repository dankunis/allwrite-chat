//
//  AppDelegate.swift
//  AllWrite chat
//
//  Created by Daniel on 23.09.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        CommunicationHandler.communicationHandler.startCommunications()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        CommunicationHandler.communicationHandler.stopCommunications()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        CommunicationHandler.communicationHandler.startCommunications()
    }
}
