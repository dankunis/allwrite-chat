//
//  CoreDataFetchRequestsTemplates.swift
//  AllWrite chat
//
//  Created by Daniel on 14.11.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import CoreData

class CoreDataFetchRequestsTemplates {
    static func getConversationByIdFetchRequest(identifier: String) -> NSFetchRequest<ConversationEntity> {
        let predicate = NSPredicate(format: "conversationID == %@", identifier)
        let fetchRequest = NSFetchRequest<ConversationEntity>(entityName: String(describing: ConversationEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
    
    static func getNotEmptyOnlineConversationsFetchRequest() -> NSFetchRequest<ConversationEntity> {
        let predicate = NSPredicate(format: "online == true && messages@.count > 0")
        let fetchRequest = NSFetchRequest<ConversationEntity>(entityName: String(describing: ConversationEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
    
    static func getOnlineConversationsFetchRequest() -> NSFetchRequest<ConversationEntity> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(ConversationEntity.hasUnreadMessages), ascending: false)
        let predicate = NSPredicate(format: "online == true")
        let fetchRequest = NSFetchRequest<ConversationEntity>(entityName: String(describing: ConversationEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    
    static func getMessagesFromConversationFetchRequest(identifier: String) -> NSFetchRequest<MessageEntity> {
        let sortDescriptor = NSSortDescriptor(key: #keyPath(MessageEntity.date), ascending: true)
        let predicate = NSPredicate(format: "conversation.messageID == %@", identifier)
        let fetchRequest = NSFetchRequest<MessageEntity>(entityName: String(describing: MessageEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    
    static func getOnlineUsersFetchRequest() -> NSFetchRequest<UserProfileDataEntity> {
        let predicate = NSPredicate(format: "conversation.online == true")
        let fetchRequest = NSFetchRequest<UserProfileDataEntity>(entityName: String(describing: UserProfileDataEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
    
    static func getUserByIdFetchRequest(identifier: String) -> NSFetchRequest<UserProfileDataEntity> {
        let predicate = NSPredicate(format: "identifier == %@", identifier)
        let fetchRequest = NSFetchRequest<UserProfileDataEntity>(entityName: String(describing: UserProfileDataEntity.self))
        
        fetchRequest.predicate = predicate
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
}
