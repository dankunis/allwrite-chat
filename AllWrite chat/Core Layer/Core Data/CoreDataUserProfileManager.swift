//
//  CoreDataUserProfileManager.swift
//  AllWrite chat
//
//  Created by Daniel on 14.11.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataUserProfileManager {
    static let shared = CoreDataUserProfileManager()
    
    private let coreData = CoreDataCustomStack()
    
    private let queue = DispatchQueue(label: "queue.concurrent", attributes: .concurrent)
    
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let profilePicFileName = "profilePic"
    
    private var userProfileDataEntity: UserProfileDataEntity? {
        let request = NSFetchRequest<UserProfileDataEntity>(entityName: String(describing: UserProfileDataEntity.self))
        let result = try? coreData.backgroundManagedObjectContext.fetch(request)
        if let userProfileDataEntity = result?.last {
            return userProfileDataEntity
        } else {
            let newUserProfileDataEntity = NSEntityDescription.insertNewObject(forEntityName: String(describing: UserProfileDataEntity.self), into: coreData.backgroundManagedObjectContext) as? UserProfileDataEntity
            return newUserProfileDataEntity
        }
    }
    
    private init() {}
    
    private func saveName(name: String) -> Bool {
        //        print(userProfileDataEntity?.userName ?? "old")
        userProfileDataEntity?.userName = name
        if let _ = try? coreData.backgroundManagedObjectContext.save() {
            return true
        } else {
            return false
        }
    }
    
    private func saveDescription(description: String?) -> Bool {
        userProfileDataEntity?.userDescription = description
        if let _ = try? coreData.backgroundManagedObjectContext.save() {
            return true
        } else {
            return false
        }
    }
    
    private func saveProfilePic(profilePic: UIImage) -> Bool {
        let profilePicURL = documentsPath.appendingPathComponent(profilePicFileName)
        
        if let data = UIImageJPEGRepresentation(profilePic, 1.0), let _ = try? data.write(to: profilePicURL) {
            userProfileDataEntity?.profilePic = profilePicFileName
            if let _ = try? coreData.backgroundManagedObjectContext.save() {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    func readUserProfileData(handler: @escaping (Bool, UserProfileData?) -> ()) {
        queue.async {
            let userProfileData = UserProfileData(username: "", description: "", profilePic: nil)
            
            if let username = self.userProfileDataEntity?.userName {
                userProfileData.username = username
            }
            
            if let description = self.userProfileDataEntity?.userDescription {
                userProfileData.description = description
            }
            
            if let profilePicFileName = self.userProfileDataEntity?.profilePic {
                let profilePicURL = self.documentsPath.appendingPathComponent(profilePicFileName)
                
                if let data = try? Data(contentsOf: profilePicURL), let profilePic = UIImage(data: data) {
                    userProfileData.profilePic = profilePic
                }
            }
            
            handler(true, userProfileData)
        }
    }
    
    func saveUserProfileData(newUserProfileData: UserProfileData, changedValues: (name: Bool, description: Bool, pic: Bool), handler: @escaping (Bool) -> Void) {
        var isSuccessful = true
        queue.async {
            if changedValues.name {
                isSuccessful = self.saveName(name: newUserProfileData.username)
            }
            
            if changedValues.description {
                isSuccessful = self.saveDescription(description: newUserProfileData.description)
            }
            
            if changedValues.pic {
                isSuccessful = self.saveProfilePic(profilePic: newUserProfileData.profilePic)
            }
            
            handler(isSuccessful)
        }
    }
}
