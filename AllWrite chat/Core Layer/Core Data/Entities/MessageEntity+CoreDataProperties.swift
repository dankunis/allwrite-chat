//
//  MessageEntity+CoreDataProperties.swift
//  AllWrite chat
//
//  Created by Daniel on 14.11.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//
//

import Foundation
import CoreData


extension MessageEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MessageEntity> {
        return NSFetchRequest<MessageEntity>(entityName: "MessageEntity")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var isSent: Bool
    @NSManaged public var messageID: String?
    @NSManaged public var messageText: String?
    @NSManaged public var conversation: ConversationEntity?

}
