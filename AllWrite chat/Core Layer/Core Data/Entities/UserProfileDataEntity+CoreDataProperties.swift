//
//  UserProfileDataEntity+CoreDataProperties.swift
//  AllWrite chat
//
//  Created by Daniel on 14.11.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//
//

import Foundation
import CoreData

extension UserProfileDataEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfileDataEntity> {
        return NSFetchRequest<UserProfileDataEntity>(entityName: "UserProfileDataEntity")
    }

    @NSManaged public var profilePic: String?
    @NSManaged public var userDescription: String?
    @NSManaged public var userName: String?
    @NSManaged public var conversation: ConversationEntity?

}
