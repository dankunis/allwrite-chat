//
//  CommunicationService.swift
//  AllWrite chat
//
//  Created by Daniel on 28.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CommunicationService: NSObject, ICommunicationService {
    static let communicationService = CommunicationService()
    
    var delegate: CommunicationServiceDelegate?
    
    var online: Bool = false {
        didSet {
            if online {
                advertiser.startAdvertisingPeer()
                browser.startBrowsingForPeers()
            } else {
                advertiser.stopAdvertisingPeer()
                browser.stopBrowsingForPeers()
            }
        }
    }
    
    private var peer: MCPeerID
    private var session: MCSession
    private var advertiser: MCNearbyServiceAdvertiser
    private var browser: MCNearbyServiceBrowser
    
    private var foundPeers: [MCPeerID : Peer] = [:]
    
    override init() {
        let userName = UserProfileDataInStorage.userProfileDataInStorage.getUserProfileData().username
        let discoveryInfo = ["userName": userName]
        let serviceType = "tinkoff-chat"
        
        peer = MCPeerID(displayName: UIDevice.current.name)
        session = MCSession(peer: peer)
        advertiser = MCNearbyServiceAdvertiser(peer: peer, discoveryInfo: discoveryInfo, serviceType: serviceType)
        browser = MCNearbyServiceBrowser(peer: peer, serviceType: serviceType)
        
        super.init()
        
        session.delegate = self
        advertiser.delegate = self
        browser.delegate = self
        
        advertiser.startAdvertisingPeer()
        browser.startBrowsingForPeers()
    }
    
    func send(_ message: Message, to peer: Peer) {
        let onlineConversations = StoredConversations.storedConversations.getOnlineConversations()
        guard
            let index = onlineConversations.index(where: { $0.peer.identifier == peer.identifier }),
            let peer = foundPeers.first(where: { (key, value) in value.identifier == peer.identifier })?.key,
            let data = messageToData(message)
        else {
            return
        }
//        print("\nI'm sending\n")
        try? session.send(data, toPeers: [peer], with: .reliable)
        
        let conversation = onlineConversations[index]
        
        StoredMessages.storedMessages.updateStoredMessages(new: message, in: conversation)
        StoredConversations.storedConversations.updateSpecificConversation(in: .online, at: index, with: conversation)
    }
    
    private func convertToPeer(_ mcPeer: MCPeerID, userName: String) -> Peer {
        return Peer(identifier: String(describing: mcPeer), text: userName)
    }
    
    private func dataToMessage(_ data: Data) -> Message? {
        guard let decoded = try? JSONSerialization.jsonObject(with: data, options: []),
            let json = decoded as? [AnyHashable: Any],
            let identifier = json["messageId"] as? String,
            let text = json["text"] as? String
        else {
            return nil
        }
        return Message(identifier: identifier, text: text, contentType: .textMessage, directionType: .received)
    }
    
    private func messageToData(_ message: Message) -> Data? {
        let json = [
            "messageId": message.identifier,
            "text": message.text,
            "eventType": message.contentType.rawValue
        ]
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
    
    func checkIfConnected(peer: Peer) -> Bool {
        guard
            let peer = foundPeers.first(where: { (key, value) in value.identifier == peer.identifier })?.key
        else {
            return false
        }

        return session.connectedPeers.contains(peer)
    }
}

// MARK: - MCSessionDelegate

extension CommunicationService: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        guard
            let peer = foundPeers[peerID],
            let message = dataToMessage(data)
        else {
            return
        }
        delegate?.communicationService(self, didReceiveMessage: message, from: peer)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    
}

// MARK: - MCNearbyServiceBrowserDelegate

extension CommunicationService: MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        guard let userName = info?["userName"],
            !foundPeers.keys.contains(peerID)
        else {
            return
        }
        
        let peer = convertToPeer(peerID, userName: userName)
        foundPeers[peerID] = peer
        
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 30)
        self.delegate?.communicationService(self, didFoundPeer: peer)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        guard let peer = foundPeers[peerID] else {
            return
        }
        delegate?.communicationService(self, didLostPeer: peer)
        foundPeers.removeValue(forKey: peerID)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("\nERROR: \(error.localizedDescription)\n")
        self.delegate?.communicationService(self, didNotStartBrowsingForPeers: error)
    }
}

// MARK: - MCNearbyServiceAdvertiserDelegate

extension CommunicationService: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        guard let peer = foundPeers[peerID] else {
            return
        }
        delegate?.communicationService(self, didReceiveInviteFromPeer: peer) { isSuccessfull in
            invitationHandler(isSuccessfull, session)
        }
        
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print("\nERROR: \(error.localizedDescription)\n")
        delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }
}
