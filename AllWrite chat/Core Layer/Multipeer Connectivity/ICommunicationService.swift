//
//  ICommunicationService.swift
//  AllWrite chat
//
//  Created by Daniel on 28.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    
    // Делегат сервиса
    var delegate: CommunicationServiceDelegate? { get set }
    /// Онлайн/Не онлайн
    var online: Bool { get set }
    /// Отправляет сообщение участнику
    func send(_ message: Message, to peer: Peer)
}
