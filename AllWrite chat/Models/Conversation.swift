//
//  ConversationPreview.swift
//  AllWrite chat
//
//  Created by Daniel on 29.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

class Conversation {
    var peer: Peer
    var name: String
    var message: String?
    var date: Date?
    var online: Bool
    var hasUnreadMessages: Bool
    
    init(peer: Peer, name: String, message: String?, date: Date?, online: Bool, hasUnreadMessages: Bool) {
        self.peer = peer
        self.name = name
        self.message = message
        self.date = date
        self.online = online
        self.hasUnreadMessages = hasUnreadMessages
    }
}
