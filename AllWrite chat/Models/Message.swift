//
//  Message.swift
//  AllWrite chat
//
//  Created by Daniel on 28.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

struct Message {
    enum ContentType: String {
        case textMessage = "TextMessage"
    }

    enum DirectionType {
        case sent
        case received
    }
    
    let identifier: String
    let text: String
    let contentType: ContentType
    let directionType: DirectionType
}

func generateMessageId() -> String {
    let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))"
    let converted = string.data(using: .utf8)?.base64EncodedString()
    
    return converted ?? ""
}
