//
//  Peer.swift
//  AllWrite chat
//
//  Created by Daniel on 28.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

struct Peer {
    let identifier: String
    let text: String
}
