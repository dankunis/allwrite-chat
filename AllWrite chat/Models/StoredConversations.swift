//
//  ConversationsInStorage.swift
//  AllWrite chat
//
//  Created by Daniel on 29.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol StoredConversationsDelegate {
    func conversationsWereUpdated()
}

class StoredConversations {
    static let storedConversations = StoredConversations()
    
    enum conversationListType {
        case online, offline
    }
    
    private var offlineConversations: [Conversation] = []
    private var onlineConversations: [Conversation] = []
    
    var delegate: StoredConversationsDelegate?
    
    func getOnlineConversations() -> [Conversation] {
        return onlineConversations
    }
    
    func getOfflineConversations() -> [Conversation] {
        return offlineConversations
    }
    
    func addToOnlineConversations(peer: Peer) {
        var conversation: Conversation
        if let index = offlineConversations.index(where: { $0.peer.identifier == peer.identifier }) {
            conversation = offlineConversations[index]
            offlineConversations.remove(at: index)
        } else {
            conversation = Conversation(peer: peer, name: peer.text, message: nil, date: nil, online: true, hasUnreadMessages: false)
        }
        onlineConversations.append(conversation)
        delegate?.conversationsWereUpdated()
    }
    
    func addToOfflineConversations(peer: Peer) {
        var conversation: Conversation
        if let index = onlineConversations.index(where: { $0.peer.identifier == peer.identifier }) {
            conversation = onlineConversations[index]
            onlineConversations.remove(at: index)
        } else {
            conversation = Conversation(peer: peer, name: peer.text, message: nil, date: nil, online: false, hasUnreadMessages: false)
        }
        offlineConversations.append(conversation)
        delegate?.conversationsWereUpdated()
    }
    
    func updateSpecificConversation(in conversationList: conversationListType, at index: Int, with conversation: Conversation) {
        switch conversationList {
        case .offline:
            offlineConversations[index] = conversation
        default:
            onlineConversations[index] = conversation
        }
    }
}
