//
//  MessageInStorage.swift
//  AllWrite chat
//
//  Created by Daniel on 29.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol StoredMessagesDelegate {
    func messagesWereUpdated()
}

class StoredMessages {
    static let storedMessages = StoredMessages()
    
    var delegate: StoredMessagesDelegate?
    
    private var storedMessages: [String : [Message]] = [:]
    
    func getStoredMessages(for conversation: Conversation) -> [Message] {
        guard let result = storedMessages[conversation.peer.identifier] else {
            return []
        }
        return result
    }
    
    func updateStoredMessages(new message: Message, in conversation: Conversation) {
        if (storedMessages[conversation.peer.identifier]?.append(message)) == nil {
            storedMessages[conversation.peer.identifier] = [message]
//            print("new message: \(storedMessages[conversation.peer.identifier])")
        }
//        print("\nSomething went wrong\n")
        delegate?.messagesWereUpdated()
    }
}
