//
//  UserInfo.swift
//  AllWrite chat
//
//  Created by Daniel on 21.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class UserProfileData {
    var username: String
    var description: String
    var profilePic: UIImage
    
    init(username: String = "", description: String = "", profilePic: UIImage? = nil) {
        self.username = username
        self.description = description
        self.profilePic = profilePic ?? #imageLiteral(resourceName: "placeholder-user")
    }
}
