//
//  UserProfileDataInStorage.swift
//  AllWrite chat
//
//  Created by Daniel on 22.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class UserProfileDataInStorage {
    static let userProfileDataInStorage = UserProfileDataInStorage()
    
    let usernamePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("username")
    let descriptionPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("description")
    let profilePicPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("profilePic")
    
    func getUserProfileData() -> UserProfileData {
        let userProfileData = UserProfileData(username: "", description: "", profilePic: nil)
        
        if let username = try? String(contentsOf: usernamePath, encoding: .utf8) {
            userProfileData.username = username
        }
        
        if let description = try? String(contentsOf: descriptionPath, encoding: .utf8) {
            userProfileData.description = description
        }
        
        if let data = try? Data(contentsOf: profilePicPath), let image = UIImage(data: data) {
            userProfileData.profilePic = image
        }
        
        return userProfileData
    }
    
    func saveUsername(name: String) -> Bool {
        if let _ = try? name.write(to: usernamePath, atomically: false, encoding: .utf8) {
            return true
        } else {
            return false
        }
    }
    
    func saveDescription(description: String) -> Bool {
        if let _ = try? description.write(to: descriptionPath, atomically: false, encoding: .utf8) {
            return true
        } else {
            return false
        }
    }
    
    func saveProfilePic(image: UIImage) -> Bool {
        if let data = UIImageJPEGRepresentation(image, 1.0) {
            if let _ = try? data.write(to: profilePicPath) {
                return true
            }
        }
        return false
    }
}
