//
//  ConversationViewController.swift
//  AllWrite chat
//
//  Created by Daniel on 07.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var conversationTableView: UITableView!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var newMessageTextField: UITextField!
    
    // MARK: Private properties
    
    private var messages: [Message] = []
    public var conversation: Conversation!
    
    private let receivedMessageCellIdentifier = String(describing: ReceivedMessageCell.self)
    private let sentMessageCellIdentifier = String(describing: SentMessageCell.self)
    
    // MARK: - Private methods
    
    func confugureSendButton(newState: Bool) {
        sendMessageButton.isEnabled = newState
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommunicationHandler.communicationHandler.delegate = self
        connectionStatusChanged(peer: conversation.peer)
        
        StoredMessages.storedMessages.delegate = self
        messages = StoredMessages.storedMessages.getStoredMessages(for: conversation)
        
        conversationTableView.register(UINib(nibName: receivedMessageCellIdentifier, bundle: nil), forCellReuseIdentifier: receivedMessageCellIdentifier)
        conversationTableView.register(UINib(nibName: sentMessageCellIdentifier, bundle: nil), forCellReuseIdentifier: sentMessageCellIdentifier)
        
        conversationTableView.dataSource = self
        
        conversationTableView.rowHeight = UITableViewAutomaticDimension
        conversationTableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        StoredMessages.storedMessages.delegate = nil
        CommunicationHandler.communicationHandler.delegate = nil
        
    }
    
    // MARK: - IBActions
    
    @IBAction func sendMessage(_ sender: UIButton) {
        guard let newMessageText = newMessageTextField.text else {
            return
        }
        let message = Message(identifier: generateMessageId(), text: newMessageText, contentType: .textMessage, directionType: .sent)
        CommunicationService.communicationService.send(message, to: conversation.peer)
        self.newMessageTextField.text = ""
        self.confugureSendButton(newState: false)
    }
    
    @IBAction func didChangeNewMessageText(_ sender: UITextField) {
        if !(newMessageTextField.text?.isEmpty ?? true) {
            connectionStatusChanged(peer: conversation.peer)
        }
    }
}

// MARK: - UITableViewDataSource

extension ConversationViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        print(message.directionType)
        
        if message.directionType == .sent {
            guard
                let cell = tableView.dequeueReusableCell(withIdentifier: sentMessageCellIdentifier, for: indexPath) as? SentMessageCell
            else {
                return UITableViewCell()
            }
            print("here")
            cell.messageText = message.text
            return cell
        } else {
            guard
                let cell = tableView.dequeueReusableCell(withIdentifier: receivedMessageCellIdentifier, for: indexPath) as? ReceivedMessageCell
            else {
                return UITableViewCell()
            }
            cell.messageText = message.text
            return cell
        }
    }
}

// MARK: - StoredMessagesDelegate

extension ConversationViewController: StoredMessagesDelegate {
    func messagesWereUpdated() {
        self.messages = StoredMessages.storedMessages.getStoredMessages(for: self.conversation)
        self.conversation.hasUnreadMessages = false
//        print("\nreally?\n")
        DispatchQueue.main.async {
            self.conversationTableView.reloadData()
//            print("\nerror\n")
        }
    }
}

extension ConversationViewController: CurrentPeerChangesConnectionStatus {
    func connectionStatusChanged(peer: Peer) {
        if peer.identifier == conversation.peer.identifier {
            let isConnected = CommunicationService.communicationService.checkIfConnected(peer: peer)
//            print("\nCurrent text: \(newMessageTextField.text ?? "no text yet")\n")
//            print("\nIs connected: \(isConnected)\n")
            if isConnected && !(newMessageTextField.text?.isEmpty ?? true) {
                DispatchQueue.main.async {
                    self.confugureSendButton(newState: true)
                }
            } else {
                DispatchQueue.main.async {
                    self.confugureSendButton(newState: false)
                }
            }
        }
    }
}
