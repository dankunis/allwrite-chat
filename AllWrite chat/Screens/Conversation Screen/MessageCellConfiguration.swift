//
//  MessageCellConfiguration.swift
//  AllWrite chat
//
//  Created by Daniel on 07.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol MessageCellConfiguration : class {
    var messageText : String? {get set}
}
