//
//  SentMessageCell.swift
//  AllWrite chat
//
//  Created by Daniel on 11.11.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class SentMessageCell: UITableViewCell, MessageCellConfiguration {

    @IBOutlet weak var messageTextLabel: UILabel!
    @IBOutlet weak var messageBackground: UIView!
    
    var messageText: String? = "" {
        didSet {
            messageTextLabel.text = messageText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageBackground.layer.cornerRadius = 10
    }
}
