//
//  ConversationsListCell.swift
//  AllWrite chat
//
//  Created by Daniel on 03.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

// MARK: ConversationsListCellConfigurationProtocol

protocol ConversationsListCellConfigurationProtocol {
    var name: String? { get set }
    var message: String? { get set }
    var date: Date? { get set }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}

class ConversationsListCell: UITableViewCell, ConversationsListCellConfigurationProtocol {
    
    // MARK: IBOutlets

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastMessagePreview: UILabel!
    @IBOutlet weak var lastMessageDate: UILabel!
    @IBOutlet weak var unreadMessageBadge: UIView!
    @IBOutlet weak var messagePreviewConstraintWithoutUnreadMessageBadge: NSLayoutConstraint!
    
    func configure(with conversation: Conversation) {
        name = conversation.name
        message = conversation.message
        date = conversation.date
        online = conversation.online
        hasUnreadMessages = conversation.hasUnreadMessages
    }
    
    // MARK: Properties
    
    var name: String? {
        didSet {
            userNameLabel.text = name
        }
    }
    
    var message: String? {
        didSet {
            if message != nil {
                lastMessagePreview.text = message
                lastMessagePreview.font = UIFont.systemFont(ofSize: lastMessagePreview.font.pointSize)
            } else {
                lastMessagePreview.text = "No messages yet"
                lastMessagePreview.font = UIFont.italicSystemFont(ofSize: lastMessagePreview.font.pointSize)
            }
        }
    }
    
    var date: Date? {
        didSet {
            guard let date = date else {
                lastMessageDate.text = ""
                return
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = NSCalendar.current.isDateInToday(date) ? "HH:mm" : "dd MMMM"
            lastMessageDate.text = dateFormatter.string(from: date)
        }
    }
    
    var online: Bool = false {
        didSet {
            backgroundColor = online
                ? UIColor(red: 1.0, green: 1.0, blue: 0.7, alpha: 0.7)
                : UIColor.clear
        }
    }
    
    
    var hasUnreadMessages: Bool = false {
        didSet {
            unreadMessageBadge.isHidden = !hasUnreadMessages
            messagePreviewConstraintWithoutUnreadMessageBadge?.isActive = !hasUnreadMessages
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        unreadMessageBadge.layer.cornerRadius = unreadMessageBadge.frame.width / 2
    }
}
