//
//  ConversationsListViewController.swift
//  AllWrite chat
//
//  Created by Daniel on 03.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class ConversationsListViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var conversationsTableView: UITableView!
    
    // MARK: - Private properties
    
    private let identifier = String(describing: ConversationsListCell.self)
    
    private let conversationsListSections = ["Online"] //, "History"]
    private var onlineConversations: [Conversation] = []
    private var offlineConversations: [Conversation] = []
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        conversationsTableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        
        conversationsTableView.dataSource = self
        conversationsTableView.delegate = self
        
        conversationsTableView.rowHeight = 70
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        conversationsWereUpdated()
        
        StoredConversations.storedConversations.delegate = self
    }
    
    // MARK: - Private methods
    
    func sortConversationsList(lhs: Conversation, rhs: Conversation) -> Bool {
        if let firstDate = lhs.date, let secondDate = rhs.date {
            return firstDate.timeIntervalSince1970 > secondDate.timeIntervalSince1970
        } else if lhs.date != nil {
            return true
        } else if rhs.date != nil {
            return false
        } else {
            return lhs.name > rhs.name
        }
    }
    
    // MARK: - Actions
    
    @IBAction func profileButtonTapped(_ sender: UIBarButtonItem) {
        if let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateInitialViewController() {
            self.present(vc, animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource

extension ConversationsListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return conversationsListSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if conversationsListSections[section] == "Online" {
            return onlineConversations.count
        } else if conversationsListSections[section] == "History" {
            return offlineConversations.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return conversationsListSections[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ConversationsListCell else {
            return UITableViewCell()
        }
        
        let conversationPreview: Conversation
        
        if conversationsListSections[indexPath.section] == "Online" {
            conversationPreview = onlineConversations[indexPath.row]
        } else {
            conversationPreview = offlineConversations[indexPath.row]
        }
        
        cell.configure(with: conversationPreview)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ConversationsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? ConversationsListCell else {
            return
        }
        
        let conversation: Conversation
        
        if conversationsListSections[indexPath.section] == "Online" {
            conversation = onlineConversations[indexPath.row]
        } else {
            conversation = offlineConversations[indexPath.row]
        }
        
        if let vc = UIStoryboard(name: "Conversation", bundle: nil).instantiateInitialViewController() as? ConversationViewController {
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.title = cell.name
            vc.conversation = conversation
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - ConversationsInStorageDelegate

extension ConversationsListViewController: StoredConversationsDelegate {
    func conversationsWereUpdated() {
        onlineConversations = StoredConversations.storedConversations.getOnlineConversations().sorted(by: sortConversationsList)
        offlineConversations = StoredConversations.storedConversations.getOfflineConversations().sorted(by: sortConversationsList)
        DispatchQueue.main.async {
            self.conversationsTableView.reloadData()
        }
    }
}
