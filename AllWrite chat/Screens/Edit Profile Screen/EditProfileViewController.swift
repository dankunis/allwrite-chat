//
//  EditProfileViewController.swift
//  AllWrite chat
//
//  Created by Daniel on 21.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var userDesriptionTextView: UITextView!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var changeProfilePicButton: UIButton!
    
    // MARK: - Private properties
    
    private var userProfileData: UserProfileData!
    private var usernameChanged: Bool = false
    private var descriptionChanged: Bool = false
    private var profilePicChanged: Bool = false
    private var lastSaveViaGCD: Bool = true
    
    private let dataManager = CoreDataUserProfileManager.shared
    
    // MARK: - Private methods
    
    private func checkForChanges() {
        if (usernameChanged || descriptionChanged || profilePicChanged) {
            configureSaveButton(newState: true)
        }
    }
    
    private func configureSaveButton(newState: Bool) {
        saveChangesButton.isEnabled = newState
    }
    
    private func readUserProfiledata() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.configureSaveButton(newState: false)
        }
        dataManager.readUserProfileData { [weak self] (isSuccessful, userProfileData) in
            if isSuccessful {
                if let userProfileData = userProfileData {
                    self?.userProfileData = userProfileData
                } else {
                    DispatchQueue.main.async {
                        self?.readingUserProfileDataError()
                    }
                    return
                }
                DispatchQueue.main.async {
                    self?.updateUserProfileData()
                    self?.activityIndicator.stopAnimating()
                }
            } else {
                DispatchQueue.main.async {
                    self?.readingUserProfileDataError()
                }
            }
        }
    }
    
    private func saveUserProfileData() {
        configureSaveButton(newState: false)
        activityIndicator.startAnimating()
        let newUserProfileData = UserProfileData(username: usernameTextField.text!, description: userDesriptionTextView.text, profilePic: profilePicImageView.image)
        dataManager.saveUserProfileData(newUserProfileData: newUserProfileData, changedValues: (name: usernameChanged, description: descriptionChanged, pic: profilePicChanged)) { [weak self] isSuccessful in
            if isSuccessful {
                DispatchQueue.main.async{ [weak self] in
                    self?.successfulSaveAlert()
                    self?.usernameChanged = false
                    self?.descriptionChanged = false
                    self?.profilePicChanged = false
                }
                self?.readUserProfiledata()
            } else {
                DispatchQueue.main.async {
                    self?.unsuccessfullSaveAlert()
                }
            }
        }
    }
    
    private func updateUserProfileData() {
        usernameTextField.text = userProfileData.username
        userDesriptionTextView.text = userProfileData.description
        profilePicImageView.image = userProfileData.profilePic
    }
    
    private func successfulSaveAlert() {
        let alert = UIAlertController(title: "Данные сохранены", message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func unsuccessfullSaveAlert() {
        let alert = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        let retry = UIAlertAction(title: "Повторить", style: .default) {_ in
            self.saveChanges(self.saveChangesButton)
        }
        alert.addAction(ok)
        alert.addAction(retry)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func readingUserProfileDataError() {
        activityIndicator.stopAnimating()
        let alert = UIAlertController(title: "Ошибка", message: "Что-то пошло не так. Попробуйте позже.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openPhotoLibrary(_ action: UIAlertAction) {
        if !UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            accessDeniedAlert(message: "Пожалуйста, разрешите приложению использовать ваши фото.")
            return
        }
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func openCamera(_ action: UIAlertAction) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            accessDeniedAlert(message: "Пожалуйста, разрешите приложению использовать вашу камеру.")
            return
        }
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .camera
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func accessDeniedAlert(message: String) {
        let cameraAlert = UIAlertController(title: "Ошибка доступа", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)
        
        cameraAlert.addAction(okAction)
        
        present(cameraAlert, animated: true, completion: nil)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userDesriptionTextView.delegate = self
        
        readUserProfiledata()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let cornerRadius = self.changeProfilePicButton.bounds.size.width / 2
        changeProfilePicButton.layer.cornerRadius = cornerRadius
        profilePicImageView.layer.cornerRadius = cornerRadius
        
        let insetDistance = cornerRadius * 0.4
        changeProfilePicButton.imageEdgeInsets = UIEdgeInsetsMake(insetDistance, insetDistance, insetDistance, insetDistance)
    }
    
    // MARK: - IBActions
    
    @IBAction func changeProfilePic(_ sender: UIButton) {
        let choosePicActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction(title: "Сделать снимок", style: .default, handler: self.openCamera)
        
        let chooseFromLibrary = UIAlertAction(title: "Выбрать из галереи", style: .default, handler: self.openPhotoLibrary)
        
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        choosePicActionSheet.addAction(takePhoto)
        choosePicActionSheet.addAction(chooseFromLibrary)
        choosePicActionSheet.addAction(cancel)
        
        present(choosePicActionSheet, animated: true, completion: nil)
    }
    
    @IBAction func userNameChanged(_ sender: UITextField) {
        usernameChanged = (userProfileData.username != usernameTextField.text)
        checkForChanges()
    }
    
    @IBAction func saveChanges(_ sender: UIButton) {
        saveUserProfileData()
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        profilePicImageView.image = selectedImage
        profilePicChanged = (profilePicImageView.image != userProfileData.profilePic)
        checkForChanges()
        
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        descriptionChanged = (userProfileData.description != userDesriptionTextView.text)
        checkForChanges()
    }
}
