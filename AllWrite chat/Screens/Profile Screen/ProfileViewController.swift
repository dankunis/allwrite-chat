//
//  ViewController.swift
//  AllWrite chat
//
//  Created by Daniel on 23.09.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userDescriptionLabel: UILabel!
    @IBOutlet weak var profilePicView: UIImageView!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
   
    // MARK: - Private properties
    
    private var userProfileData: UserProfileData!
    
    private let dataManager = CoreDataUserProfileManager.shared
    
    // MARK: - Private methods
    
    private func readUserProfiledata() {
        self.activityIndicator.startAnimating()
        dataManager.readUserProfileData { [weak self] (isSuccessful, userProfileData) in
            if isSuccessful {
                if let userProfileData = userProfileData {
                    self?.userProfileData = userProfileData
                } else {
                    DispatchQueue.main.async {
                        self?.readingUserProfileDataError()
                    }
                    return
                }
                DispatchQueue.main.async {
                    self?.updateUserProfileData()
                    self?.activityIndicator.stopAnimating()
                }
            } else {
                DispatchQueue.main.async {
                    self?.readingUserProfileDataError()
                }
            }
        }
    }
    
    private func updateUserProfileData() {
        usernameLabel.text = userProfileData.username
        userDescriptionLabel.text = userProfileData.description
        profilePicView.image = userProfileData.profilePic
    }
    
    private func readingUserProfileDataError() {
        activityIndicator.stopAnimating()
        let alert = UIAlertController(title: "Ошибка", message: "Что-то пошло не так. Попробуйте позже.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let cornerRadius = self.profilePicView.layer.bounds.size.width / 10
        profilePicView.layer.cornerRadius = cornerRadius
        
        editProfileButton.layer.borderWidth = 1.5
        editProfileButton.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        readUserProfiledata()
    }

    // MARK: - Actions
    
    @IBAction func closeProfileScreen(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editProfile(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "EditProfile", bundle: nil).instantiateInitialViewController() as? EditProfileViewController {
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.title = "Edit Profile"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
