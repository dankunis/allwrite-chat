//
//  CommunicationHandler.swift
//  AllWrite chat
//
//  Created by Daniel on 29.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol CurrentPeerChangesConnectionStatus {
    func connectionStatusChanged(peer: Peer)
}

class CommunicationHandler: CommunicationServiceDelegate {
    static let communicationHandler = CommunicationHandler()
    
    let communicationService = CommunicationService.communicationService
    let conversationsInStorage = StoredConversations.storedConversations
    
    var delegate: CurrentPeerChangesConnectionStatus?
    
    init() {
       communicationService.delegate = self
    }
    
    func startCommunications() {
        communicationService.online = true
    }
    
    func stopCommunications() {
        communicationService.online = false
    }
    
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        conversationsInStorage.addToOnlineConversations(peer: peer)
        delegate?.connectionStatusChanged(peer: peer)
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        conversationsInStorage.addToOfflineConversations(peer: peer)
        delegate?.connectionStatusChanged(peer: peer)
    }
    
    // TODO: add UI notifications to handle these scenarios
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
        
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: (Bool) -> Void) {
        invintationClosure(true)
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
        
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
        let onlineConversations = StoredConversations.storedConversations.getOnlineConversations()
        guard
            let index = onlineConversations.index(where: { $0.peer.identifier == peer.identifier })
        else {
            return
        }
        let conversation = onlineConversations[index]
        
        conversation.date = Date()
        conversation.hasUnreadMessages = true
        conversation.message = message.text
        
        print("\nBut were they?\n")
        
        StoredMessages.storedMessages.updateStoredMessages(new: message, in: conversation)
        StoredConversations.storedConversations.updateSpecificConversation(in: .online, at: index, with: conversation)
    }
}
