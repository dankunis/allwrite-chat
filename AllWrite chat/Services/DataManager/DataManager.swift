//
//  DataManager.swift
//  AllWrite chat
//
//  Created by Daniel on 22.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import Foundation

protocol DataManager {
    func saveUserProfileData(newUserProfileData: UserProfileData, changedValues: (name: Bool, description: Bool, pic: Bool), handler: @escaping (Bool) -> Void)
    func readUserProfileData(handler: @escaping (Bool, UserProfileData?) -> Void)
}
