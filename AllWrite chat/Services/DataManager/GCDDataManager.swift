//
//  GCDDataManager.swift
//  AllWrite chat
//
//  Created by Daniel on 22.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class GCDDataManager: DataManager {
    
    private let userProfileDataInstorage = UserProfileDataInStorage.userProfileDataInStorage
    private let queue = DispatchQueue(label: "queue.concurrent", attributes: .concurrent)
    
    func saveUserProfileData(newUserProfileData: UserProfileData, changedValues: (name: Bool, description: Bool, pic: Bool), handler: @escaping (Bool) -> Void) {
        queue.async {
            var isSuccessful = true
            
            if changedValues.name {
                isSuccessful = self.userProfileDataInstorage.saveUsername(name: newUserProfileData.username)
            }
            
            if changedValues.description {
                isSuccessful = self.userProfileDataInstorage.saveDescription(description: newUserProfileData.description)
            }
            
            if changedValues.pic {
                isSuccessful = self.userProfileDataInstorage.saveProfilePic(image: newUserProfileData.profilePic)
            }
            
            handler(isSuccessful)
        }
    }
    
    func readUserProfileData(handler: @escaping (Bool, UserProfileData?)->()){
        queue.async {
            let userProfileData = self.userProfileDataInstorage.getUserProfileData()
            handler(true, userProfileData)
        }
    }
}
