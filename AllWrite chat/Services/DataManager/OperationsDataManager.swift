//
//  OperationsDataManager.swift
//  AllWrite chat
//
//  Created by Daniel on 22.10.2018.
//  Copyright © 2018 Tinkoff Fintech. All rights reserved.
//

import UIKit

class OperationsDataManager: DataManager {
    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func saveUserProfileData(newUserProfileData: UserProfileData, changedValues: (name: Bool, description: Bool, pic: Bool), handler: @escaping (Bool) -> Void) {
        let saveUserProfileDataOperation = SaveUserProfileDataOperation()
        saveUserProfileDataOperation.changedValues = changedValues
        saveUserProfileDataOperation.userProfileData = newUserProfileData
        saveUserProfileDataOperation.completionBlock = {
            handler(saveUserProfileDataOperation.isSuccessful)
        }
        queue.addOperation(saveUserProfileDataOperation)
    }
    
    func readUserProfileData(handler: @escaping (Bool, UserProfileData?) -> Void) {
        let readUserProfileDataOperation = ReadUserProfileDataOperation()
        readUserProfileDataOperation.completionBlock = {
            handler(true, readUserProfileDataOperation.userProfileData)
        }
        queue.addOperation(readUserProfileDataOperation)
    }
}

class ReadUserProfileDataOperation: Operation {
    var userProfileData: UserProfileData?
    
    override func main() {
        let userProfileDataInStorage = UserProfileDataInStorage.userProfileDataInStorage
        
        userProfileData = userProfileDataInStorage.getUserProfileData()
    }
}

class SaveUserProfileDataOperation: Operation {
    var userProfileData: UserProfileData?
    var changedValues: (name: Bool, description: Bool, pic: Bool) = (name: false, description: false, pic: false)
    var isSuccessful: Bool = true
    
    override func main() {
        if let newUserProfileData = userProfileData {
            let userProfileDataInStorage = UserProfileDataInStorage.userProfileDataInStorage
            
            if changedValues.name {
                isSuccessful = userProfileDataInStorage.saveUsername(name: newUserProfileData.username)
            }
            
            if changedValues.description {
                isSuccessful = userProfileDataInStorage.saveDescription(description: newUserProfileData.description)
            }
            
            if changedValues.pic {
                isSuccessful = userProfileDataInStorage.saveProfilePic(image: newUserProfileData.profilePic)
            }
        }
    }
}
